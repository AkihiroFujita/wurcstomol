package obj;

import java.util.LinkedHashMap;

public class Atom {

	public int num = -1;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String symbol = "";
	public Vector3D Vector3D = new Vector3D();
	public LinkedHashMap<Integer, Integer> list_tpo = new LinkedHashMap<Integer, Integer>();
	public LinkedHashMap<Integer, Integer> list_bind = new LinkedHashMap<Integer, Integer>();

	public void addLink(int i, int tpo , int bind) {
		list_tpo.put(i, tpo);
		list_bind.put(i, bind);
			return;
	}

	public LinkedHashMap<Integer, Integer> getLink() {
		return list_tpo;
	}
	public void setLink(LinkedHashMap<Integer, Integer> list_tpo) {
		this.list_tpo = list_tpo;
	}

	public LinkedHashMap<Integer, Integer> getBind() {
		return list_bind;
	}
	public void setBind(LinkedHashMap<Integer, Integer> list_bind) {
		this.list_bind = list_bind;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Vector3D getVector3D() {
		return Vector3D;
	}

	public void setVector3D(Vector3D vector3d) {
		Vector3D = vector3d;
	}

	public Atom(String valueOf, int i, Vector3D v) {
		symbol = valueOf;
		num = i;
		Vector3D = v;
	}

}
