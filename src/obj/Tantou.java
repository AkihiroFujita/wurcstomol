package obj;

import java.util.ArrayList;
import java.util.List;

public class Tantou {
	static ArrayList<Atom> tantou = new ArrayList<Atom>();
	static int[] position = new int[20];

	public int getPosition(int pos) {
		return position[pos - 1];
	}

	public ArrayList<Atom> getTantou() {
		return tantou;
	}

	public static List<Atom> makeTantou(String in, String skc, char ano) {
		int num = 1;
		if (in.equals("1-5")) {
			tantou.add(new Atom("O", 0 + num, new Vector3D(0d, -1.29d, 0d)));
			tantou.add(new Atom("C", 1 + num, new Vector3D(1.49d, -1.29d, 0d)));
			tantou.add(new Atom("C", 2 + num, new Vector3D(2.33d, 0d, 0d)));
			tantou.add(new Atom("C", 3 + num, new Vector3D(1.49d, 1.29d, 0d)));
			tantou.add(new Atom("C", 4 + num, new Vector3D(0d, 1.29d, 0d)));
			tantou.add(new Atom("C", 5 + num, new Vector3D(-0.79d, 0d, 0d)));
			tantou.add(new Atom("C", 6 + num, new Vector3D(-2.23d, 0d, 0d)));

			tantou.get(0).addLink(1 + num, 0, 1);
			tantou.get(0).addLink(5 + num, 0, 1);
			tantou.get(1).addLink(2 + num, 0, 1);
			tantou.get(1).addLink(0 + num, 0, 1);
			tantou.get(2).addLink(3 + num, 0, 1);
			tantou.get(2).addLink(1 + num, 0, 1);
			tantou.get(3).addLink(4 + num, 0, 1);
			tantou.get(3).addLink(2 + num, 0, 1);
			tantou.get(4).addLink(5 + num, 0, 1);
			tantou.get(4).addLink(3 + num, 0, 1);
			tantou.get(5).addLink(0 + num, 0, 1);
			tantou.get(5).addLink(4 + num, 0, 1);

			int tpo = 0;

			for (int i = 0; i < skc.length(); i++) {
				if (i == 4) {
					if (skc.charAt(i) == '1') {
						tpo = 1;
					} else if (skc.charAt(i) == '2') {
						tpo = 6;
					}
					tantou.get(5).addLink(6 + num, tpo, 1);
				} else {
					if (skc.charAt(i) == 'a') {
						if (ano == 'a') {
							if (skc.charAt(4) == '1') {
								tpo = 6;
							} else if (skc.charAt(4) == '2') {
								tpo = 1;
							}
						} else if (ano == 'b') {
							if (skc.charAt(4) == '1') {
								tpo = 1;
							} else if (skc.charAt(4) == '2') {
								tpo = 6;
							}
						} else if (ano == 'x') {
							tpo = 3;
						}
						tantou.add(new Atom("O", 7 + num, new Vector3D(0d, 0d, 0d)));
						position[i] = tantou.size() - 1;
						tantou.get(1).addLink(7 + num, tpo, 1);
					} else if (skc.charAt(i) == '1') {
						tpo = 6;
						tantou.add(new Atom("O", 7 + i + num, new Vector3D(0d, 0d, 0d)));
						position[i] = tantou.size() - 1;
						tantou.get(1 + i).addLink(7 + i + num, tpo, 1);
					} else if (skc.charAt(i) == '2') {
						tpo = 1;
						tantou.add(new Atom("O", 7 + i + num, new Vector3D(0d, 0d, 0d)));
						position[i] = tantou.size() - 1;
						tantou.get(1 + i).addLink(7 + i + num, tpo, 1);
					} else if (skc.charAt(i) == 'h') {
						tantou.add(new Atom("O", 6 + i + num, new Vector3D(0d, 0d, 0d)));
						position[i] = tantou.size() - 1;
						tantou.get(1 + i).addLink(6 + i + num, 0, 1);
					} else if (skc.charAt(i) == 'm') {

					} else {
					}
				}
			}

		} else if (in.equals("1-4") || in.equals("2-5")) {
			tantou.add(new Atom("O", 0 + num, new Vector3D(0d, 0d, 0d)));
			tantou.add(new Atom("C", 1 + num, new Vector3D(0d, 0d, 0d)));
			tantou.add(new Atom("C", 2 + num, new Vector3D(0d, 0d, 0d)));
			tantou.add(new Atom("C", 3 + num, new Vector3D(0d, 0d, 0d)));
			tantou.add(new Atom("C", 4 + num, new Vector3D(0d, 0d, 0d)));
			tantou.get(0).addLink(4 + num, 0, 1);
			tantou.get(0).addLink(1 + num, 0, 1);
			tantou.get(1).addLink(2 + num, 0, 1);
			tantou.get(1).addLink(0 + num, 0, 1);
			tantou.get(2).addLink(3 + num, 0, 1);
			tantou.get(2).addLink(1 + num, 0, 1);
			tantou.get(3).addLink(4 + num, 0, 1);
			tantou.get(3).addLink(2 + num, 0, 1);
			tantou.get(4).addLink(0 + num, 0, 1);
			tantou.get(4).addLink(3 + num, 0, 1);
		}
		return tantou;
	}

	public void addMapCode(Tantou mol, int pos, String mc) {
		System.out.println(pos);
		Atom backbornPositonAtom = mol.getTantou().get(pos);

		method.MakeModification.makeModification(mol, backbornPositonAtom, mc);

	}

}
