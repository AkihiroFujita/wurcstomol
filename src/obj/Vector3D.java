package obj;

public class Vector3D {
    public double[] crd; // (x,y,z)：座標(coordinate)

    // コンストラクタ #0
    public Vector3D() {
        crd = new double[3];
        crd[0] = 0.0d;
        crd[1] = 0.0d;
        crd[2] = 0.0d;
    }

    public Vector3D(double x, double y, double z) {
        crd = new double[3];
        crd[0] = x;
        crd[1] = y;
        crd[2] = z;
    }

	public double getx() {
		return crd[0];
	}

	public double gety() {
		return crd[1];
	}

	public double getz() {
		return crd[2];
	}

	public void setCrd(double[] crd) {
		this.crd = crd;
	}

}
