package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.wurcs.array.LIPs;
import org.glycoinfo.WURCSFramework.wurcs.array.MOD;
import org.glycoinfo.WURCSFramework.wurcs.array.UniqueRES;

import method.Error;
import method.ToMolString;
import method.Todo;
import obj.Atom;
import obj.Tantou;

public class Main {

	public static void main(String[] args)  {



		 FileReader fr = null;
		    BufferedReader br = null;
		    try {

				fr = new FileReader("C:/Users/radio/git/wurcstomol/testdata/testStructures");
		        br = new BufferedReader(fr);

		        String line;
		        while ((line = br.readLine()) != null) {
		            System.out.println(line);
		        }
		        w2mol(line);
		    } catch (FileNotFoundException e) {
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    } finally {
		        try {
		            br.close();
		            fr.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }






	}

	static ArrayList<ArrayList<Atom>> tantou = new ArrayList<ArrayList<Atom>>();
/**
	 * memo int 初期値は0 文字列 初期値は空文字列 Mapcode 修飾基 LIP 冠構造（架橋構造を含む）
	 **/
	public static void w2mol(String input) {

		// String input =
		// "WURCS=2.0/2,2,1/[a1122h-1x_1-5][21122h-1a_1-5]/1-2/a?-b1";
		// String input = "WURCS=2.0/2,2,1/[u344h][a344h-1x_1-4]/1-2/a5-b1";
		// String input = "WURCS=2.0/1,1,0/[u344h]/1/";
		// String input = "WURCS=2.0/2,2,1/[a1122h-1x_1-5][21122h-1a_1-5]/1-2/a?-b1";
		// String input = "WURCS=2.0/1,3,2/[a2122h-1b_1-5]/1-1-1/a3-b1_b3-c1";

		// String input = "WURCS=2.0/1,1,0/[a2112h-1a_1-5]/1/";
		Error.checkInput(input);

		WURCSImporter ws = new WURCSImporter();
		org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray wArray = null;
		try {
			wArray = ws.extractWURCSArray(input);
		} catch (WURCSFormatException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		LinkedList<UniqueRES> uRes = wArray.getUniqueRESs();

		for (int i = 0; i < uRes.size(); i++) {

			Tantou mol = new Tantou();
			String skc = uRes.get(i).getSkeletonCode();
			char ano = uRes.get(i).getAnomericSymbol();
			System.out.println(i + "\t" + uRes.get(i).getAnomericSymbol() + "\t" + uRes.get(i).getAnomericPosition()
					+ "\t" + uRes.get(i).getSkeletonCode());
			LinkedList<MOD> modList = uRes.get(i).getMODs();

			if (modList.size() > 0) {
				// モディフィケーションのループ
				for (int j = 0; j < modList.size(); j++) {
					String mapCode = uRes.get(i).getMODs().get(j).getMAPCode();

					// 環構造かもしれない
					if (mapCode.equals("")) {
						LinkedList<LIPs> lIPs = uRes.get(i).getMODs().get(j).getListOfLIPs();
						// 環構造
						if (lIPs.size() >= 2) {
							String lIP = String.valueOf(lIPs.get(0).getLIPs().getFirst().getBackbonePosition()) + "-"
									+ String.valueOf(lIPs.get(1).getLIPs().getLast().getBackbonePosition());
							Tantou.makeTantou(lIP, skc, ano);
						} else {// openchain
						}
					} else {// 修飾基
						// mol.addAll(Ring.makeMod(mapCode, mol.size(),
						// skc,ano));
						LinkedList<LIPs> lIPs = uRes.get(i).getMODs().get(j).getListOfLIPs();
						System.out.println("\tMap\t" + mapCode);

						//mol = Mapcode.addMapcode(mol ,mol.getPosition(lIPs.get(0).getLIPs().getLast().getBackbonePosition()),mapCode);
						mol.addMapCode(mol , mol.getPosition(lIPs.get(0).getLIPs().getLast().getBackbonePosition()) ,mapCode);
						//System.out.println(mol.getPosition(lIPs.get(0).getLIPs().getLast()
							//	.getBackbonePosition()));
					}
				}
			} else {
				Todo.ringOrStraight(modList.size());
			}
			tantou.add(mol.getTantou());
		}
		for (int k = 0; k < tantou.size(); k++) {
			// System.out.println("****\ndebug message above\n***");
			List<String> str = ToMolString.output(tantou.get(k));
			File file = new File("C:/Users/radio/Desktop/test.mol");
			PrintWriter pw = null;
			try {
				pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			pw.println("");
			pw.println("WURCStoMol this is alpha version by Akihiro Fujita");
			pw.println("");

			for (int m = 0; m < str.size(); m++) {
				pw.println(str.get(m));
			}
			pw.close();
		}
	}
}
