package method;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NotClear {
	public static boolean exactNotClear(String wurcs) {

		String str = wurcs;
		Pattern p = Pattern.compile(Pattern.quote("?"));
		Matcher m = p.matcher(str);

		return m.find();
	}
}
