package method;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import obj.Atom;

public class ToMolString {

	static Integer AtomBlockNum = 0;
	static Integer BondBlockNum = 0;

	public static List<String> output(List<Atom> mol) {
		List<String> output = new ArrayList<String>();
		for (int i = 0; i < mol.size(); i++) {
			output.add(AtomToBlock(mol.get(i)));
		}
		for (int i = 0; i < mol.size(); i++) {
			String bond = AtomToBBlck(mol.get(i));
			if (!bond.equals(""))
				output.add(bond);
		}
		output.add("M  END");

		output.add(0,String.format("%3S" , AtomBlockNum)  + String.format("%3S" ,BondBlockNum)
				+ "  0  0  0  0  0  0  0  0999 V2000");

		return output;
	}
	static String AtomToBlock(Atom a) {
		String ret =   String.format("%10S" , String.format("%.4f",a.getVector3D().getx()))
				+ String.format("%10S" , String.format("%.4f",a.getVector3D().gety()))
				+ String.format("%10S" , String.format("%.4f",a.getVector3D().getz()))  + " " + a.getSymbol()
				+ "   0  0  0  0  0  0  0  0  0  0  0  0";
		AtomBlockNum++;
		return ret;
	}

	static String AtomToBBlck(Atom a) {
		String ret = "";
		LinkedHashMap<Integer,Integer> list = a.getLink();
		LinkedHashMap<Integer,Integer> list_bind = a.getBind();
		List<Integer> lkey = new ArrayList<Integer>(list.keySet());
		List<Integer> lval = new ArrayList<Integer>(list.values());
		List<Integer> bval = new ArrayList<Integer>(list_bind.values());
		for (int i=0; i < lkey.size(); i++) {
			if (a.getNum() < lkey.get(i)) {
				if(!ret.equals("")){
					ret = ret + System.getProperty("line.separator");
				}
				ret = ret + String.format("%3S",a.getNum())
						+ String.format("%3S",lkey.get(i)) + String.format("%3S",bval.get(i)) + String.format("%3S",lval.get(i)) +"  0  0  0          ";
				BondBlockNum++;
			}
		}
		return ret;
	}

}
