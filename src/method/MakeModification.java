package method;

import obj.Atom;
import obj.Tantou;
import obj.Vector3D;

public class MakeModification {
	static public void makeModification(Tantou mol, Atom backbornPositonAtom, String mc) {

		int num = mol.getTantou().get(mol.getTantou().size() - 1).getNum();

		if (mc == null) {
			System.out.println("修飾構造がありません。");
		}

		switch (mc) {
		case "*NCC/3=O":
			backbornPositonAtom.setSymbol("N");

			mol.getTantou().add(new Atom("C", num + 1, new Vector3D(0d, 0d, 0d)));
			mol.getTantou().add(new Atom("C", num + 2, new Vector3D(0d, 0d, 0d)));
			mol.getTantou().add(new Atom("O", num + 3, new Vector3D(0d, 0d, 0d)));
			backbornPositonAtom.addLink(num + 1, 0, 1);
			mol.getTantou().get(num).addLink(num + 2, 0, 1);
			mol.getTantou().get(num).addLink(num + 3, 0, 2);

			break;
		default:
			System.out.println("未定義な修飾構造です。");
		}

	}
}
